#!/bin/sh


PROGFOLDER="packaging-test-0.2"

if [ $# -ne 0 ]
then

 if [ $1 = "clean" ]
 then
  rm -r rpmbuild
  rm -r ${PROGFOLDER}
 fi

else

rm -f -r rpmbuild
rm -f -r ${PROGFOLDER}




mkdir rpmbuild &&
cd rpmbuild &&
mkdir BUILD BUILDROOT RPMS SOURCES SPECS SRPMS &&
cd .. &&

mkdir $PROGFOLDER &&
cp demo_libuv.c ${PROGFOLDER}/ &&
cp Makefile ${PROGFOLDER}/ &&
cp LICENSE ${PROGFOLDER}/ &&
cp README.md ${PROGFOLDER}/ &&
cp packaging-test.service ${PROGFOLDER}/ &&
cp demo_libuv.1 ${PROGFOLDER}/ &&

tar -cf packaging-test-v0.2.tar.gz -z ${PROGFOLDER} &&

mv packaging-test-v0.2.tar.gz rpmbuild/SOURCES/ &&

#build source package
rpmbuild -bs packaging-test.spec --define "_topdir $(pwd)/rpmbuild" &&


#build binary package
rpmbuild --rebuild rpmbuild/SRPMS/packaging-test-0.2-1.fc32.src.rpm --define "_topdir $(pwd)/rpmbuild" &&


#linters
rpmlint packaging-test.spec &&
rpmlint rpmbuild/SRPMS/packaging-test-0.2-1.fc32.src.rpm &&
rpmlint rpmbuild/RPMS/x86_64/packaging-test-0.2-1.fc32.x86_64.rpm 






fi
