Format: 3.0 (quilt)
Source: packaging-test
Binary: packaging-test
Architecture: any
Version: 0.2-1
Maintainer: Jiří Hartmann <hartmann.j@seznam.cz>
Standards-Version: 4.0.0
Build-Depends: debhelper (>= 9), debhelper-compat (= 10)
Package-List:
 packaging-test deb misc optional arch=any
Checksums-Sha1:
 606013758a634fef25bdb5ecc67730a1b80ce848 13768 packaging-test_0.2.orig.tar.gz
 2b2c25d51a31b197d32bbb21edc4bb4434030f26 936 packaging-test_0.2-1.debian.tar.xz
Checksums-Sha256:
 b5aa2d79a85fba021efc3c04735947bc74023b18e7652a77a6e2cb71933056c0 13768 packaging-test_0.2.orig.tar.gz
 a2ce0cbe37d8fb495a7173b392b7fa2edfdebcc9f8f2ed71323b1a47bcbee6ba 936 packaging-test_0.2-1.debian.tar.xz
Files:
 00bd2c1a88952d2aecf5a65a251662aa 13768 packaging-test_0.2.orig.tar.gz
 4e22e757bcc0d9aedadee2d6026f1cb1 936 packaging-test_0.2-1.debian.tar.xz
