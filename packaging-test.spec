Name:           packaging-test
Version:        0.2
Release:        1%{?dist}
Summary:        Demo SW packaging

License:        GPLv3+
URL:            https://gitlab.labs.nic.cz/knot/packaging-test/
Source0:        https://gitlab.labs.nic.cz/knot/packaging-test/-/archive/v0.2/packaging-test-v0.2.tar.gz

BuildRequires:  gcc make libuv-devel systemd


%description
This is example SW packaging description.


%global debug_package %{nil}

%prep
%autosetup


%build
%make_build


%install
mkdir -p %{buildroot}%{_bindir}
install -m 755 -s demo_libuv %{buildroot}%{_bindir}/demo_libuv

mkdir -p %{buildroot}%{_unitdir}
install -m 644 packaging-test.service %{buildroot}%{_unitdir}/packaging-test.service

mkdir -p %{buildroot}%{_mandir}/man1
install -m 644 demo_libuv.1 %{buildroot}%{_mandir}/man1

%files
%license LICENSE
/usr/bin/demo_libuv
%{_unitdir}/packaging-test.service
%{_mandir}/man1/demo_libuv.1*

%changelog
* Thu Jul 9 2020 hary777 <hartmann.j@seznam.cz> - 0.2-1
- Somefix
