#!/bin/sh


PROGFOLDER="packaging-test-0.2"

if [ $# -ne 0 ]
then

 if [ $1 = "clean" ]
 then
  rm -r $PROGFOLDER
  rm packaging-test_0.2*
 fi

else

rm -r $PROGFOLDER
rm packaging-test_0.2*







mkdir $PROGFOLDER &&
mkdir $PROGFOLDER/debian &&

cp demo_libuv.c ${PROGFOLDER}/ &&
cp Makefile ${PROGFOLDER}/ &&
cp LICENSE ${PROGFOLDER}/ &&
cp README.md ${PROGFOLDER}/ &&
cp demo_libuv.1 ${PROGFOLDER}/ &&
cp packaging-test.service ${PROGFOLDER}/debian/ &&

tar -cf packaging-test_0.2.orig.tar.gz -z ${PROGFOLDER}

cd $PROGFOLDER &&

#dch --create -v 1.0-1 --package demo-libuv &&
echo 'packaging-test (0.2-1) UNRELEASED; urgency=low

  * Initial release. (Closes: nothing)
  - ITP: packaging-test -- short description

 -- Jiří Hartmann <hartmann.j@seznam.cz>  Thu, 02 Jul 2020 23:12:04 +0200
' > debian/changelog &&




#echo "10" > debian/compat &&

echo 'Source: packaging-test
Maintainer: Jiří Hartmann <hartmann.j@seznam.cz>
Section: misc
Priority: optional
Standards-Version: 4.0.0
Build-Depends: debhelper (>= 9),
               debhelper-compat (= 10)

Package: packaging-test
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Useless package can test libuv library.
 This is extended decsription(just next line).
' > debian/control &&

echo 'Files: *
Copyright: 1970 Some Name <addr@domain.dom>
License: GPLv3+
' > debian/copyright &&


mkdir debian/source &&
echo '#!/usr/bin/make -f
%:
	dh $@

override_dh_strip:
	dh_strip --no-automatic-dbgsym

override_dh_installinit:
	true

' > debian/rules &&

echo '3.0 (quilt)' > debian/source/format &&

echo 'demo_libuv usr/bin' > debian/packaging-test.install &&

echo 'demo_libuv.1' > debian/packaging-test.manpages





debuild -us -uc


fi
