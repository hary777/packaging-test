#!/bin/sh

GREETING="Hello! ${USER} weekday is $(date +%u)"



if [ $# -eq 0 ]
then
 echo ${GREETING}

else

 #also exist nice variants with for loop
 #but some of these is bashism and definitly not POSIX compatible
 i=0
 while [ $i -lt $1 ]
 do
  i=$(( $i+1 ))
  echo ${GREETING}
 done

fi
